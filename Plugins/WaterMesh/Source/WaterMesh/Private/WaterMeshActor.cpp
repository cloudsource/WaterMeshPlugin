// Fill out your copyright notice in the Description page of Project Settings.

#include "WaterMeshPrivatePCH.h"
#include "WaterMeshActor.h"
#include "ProceduralMeshComponent.h"

// Sets default values
AWaterMeshActor::AWaterMeshActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    //几个参数的默认值
    ParamC = 0.3f;
    ParamD = 0.4f;
    ParamU = 0.05f;
    ParamT = 0.13f;

    bUseFakeNormals = false;

    MeshComplexity = 60;
    PlaneSize = 400;

    DynamicMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("DynamicMesh"));

    GenerateMesh();
}

// Called when the game starts or when spawned
void AWaterMeshActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWaterMeshActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

    UpdateMesh(DeltaTime);
}

void AWaterMeshActor::GenerateMesh()
{
    int x, y, b;
    NumFaces = 2 * MeshComplexity * MeshComplexity;
    NumVertices = (MeshComplexity + 1) * (MeshComplexity + 1);

    // 重新分配空间
    Vertices[0].SetNum(NumVertices, true);
    Vertices[1].SetNum(NumVertices, true);
    Vertices[2].SetNum(NumVertices, true);

    Normals.SetNum(NumVertices, true);
    Texcoords.SetNum(NumVertices, true);
    Triangles.SetNum(NumFaces*3, true);

    // 计算UV坐标
    for (y = 0; y <= MeshComplexity; y++)
    {
        for (x = 0; x <= MeshComplexity; x++)
        {
            FVector2D uv;
            uv.X = (float)x / MeshComplexity;
            uv.Y = 1.0f - ((float)y / MeshComplexity);
            Texcoords[y*(MeshComplexity + 1) + x + 0] = uv;
        }
    }

    // 计算索引缓冲
    for (y = 0; y < MeshComplexity; y++)
    {
        for (x = 0; x < MeshComplexity; x++)
        {
            int nIndex = (y*MeshComplexity + x) * 6;

            int p0 = y * (MeshComplexity + 1) + x;
            int p1 = y * (MeshComplexity + 1) + x + 1;
            int p2 = (y + 1) * (MeshComplexity + 1) + x;
            int p3 = (y + 1) * (MeshComplexity + 1) + x + 1;
            Triangles[nIndex +0 ] = p2;     //First Triangle
            Triangles[nIndex + 1] = p1;
            Triangles[nIndex + 2] = p0;
            Triangles[nIndex + 3] = p2;     //Second Triangle
            Triangles[nIndex + 4] = p3;
            Triangles[nIndex + 5] = p1;
        }
    }

    // 计算顶点，这里用3个缓冲区来保存动态的顶点
    for (b = 0; b < 3; b++)
    {
        for (y = 0; y <= MeshComplexity; y++)
        {
            for (x = 0; x <= MeshComplexity; x++)
            {
                int PointIndex = y*(MeshComplexity+1)+x;
                FVector VertexPos;
                VertexPos.X = (float)(x) / (float)(MeshComplexity) * (float)PlaneSize;
                VertexPos.Y = (float)(y) / (float)(MeshComplexity) * (float)PlaneSize;
                VertexPos.Z = 0;

                Vertices[b][PointIndex] = VertexPos;
            }
        }
    }

    TArray<FColor> Colors;
    Colors.SetNum(NumVertices);
    TArray<FProcMeshTangent> Tangents;
    Tangents.SetNum(NumVertices);

    CalculateNormals();

    DynamicMesh->CreateMeshSection(0,
        Vertices[0],
        Triangles,
        Normals,
        Texcoords,
        Colors,
        Tangents, false);
}

void AWaterMeshActor::CalculateNormals()
{
    int i, x, y;

    //清空 
    for (i = 0; i < NumVertices; i++)
    {
        Normals[i] = FVector::ZeroVector;
    }

    for (i = 0; i < NumFaces; i++)
    {
        int p0 = Triangles[3 * i];
        int p1 = Triangles[3 * i + 1];
        int p2 = Triangles[3 * i + 2];
        FVector v0 = Vertices[CurrentVertexIndex][p0];
        FVector v1 = Vertices[CurrentVertexIndex][p1];
        FVector v2 = Vertices[CurrentVertexIndex][p2];

        FVector diff1 = v2 - v1;
        FVector diff2 = v0 - v1;
        FVector fn = FVector::CrossProduct(diff1, diff2);

        Normals[p0] += fn;
        Normals[p1] += fn;
        Normals[p2] += fn;
    }

    // Normalize vertex normals
    for (y = 0; y < MeshComplexity; y++)
    {
        for (x = 0; x < MeshComplexity; x++)
        {
            int PointIndex = y*(MeshComplexity + 1) + x;
            Normals[PointIndex].Normalize();
        }
    }
}

void AWaterMeshActor::UpdateMesh(float DeltaSeconds)
{
    int x, y;
    CurrentVertexIndex = (CurrentVertexIndex + 1) % 3;

    TArray<FVector>& pBuf = Vertices[CurrentVertexIndex];
    TArray<FVector>& pBuf1 = Vertices[(CurrentVertexIndex + 2) % 3];
    TArray<FVector>& pBuf2 = Vertices[(CurrentVertexIndex + 1) % 3];

    float C = ParamC;
    float D = ParamD;
    float U = ParamU;
    float T = ParamT;

    float TERM1 = (4.0f - 8.0f*C*C*T*T / (D*D)) / (U*T + 2);
    float TERM2 = (U*T - 2.0f) / (U*T + 2.0f);
    float TERM3 = (2.0f * C*C*T*T / (D*D)) / (U*T + 2);

    for (y = 1; y < MeshComplexity; y++)
    {
        int IndexRow = y*(MeshComplexity + 1);
        int IndexRow1 = y*(MeshComplexity + 1);
        int IndexRow1Up = (y - 1) * (MeshComplexity + 1);
        int IndexRow1Down = (y + 1) * (MeshComplexity + 1);
        int IndexRow2 = y*(MeshComplexity + 1);

        for (x = 1; x < MeshComplexity; x++)
        {
            pBuf[IndexRow + x].Z = TERM1 * pBuf1[IndexRow1 + x].Z
                + TERM2 * pBuf2[IndexRow2 + x].Z
                + TERM3
                * (pBuf1[IndexRow1 + x - 1].Z + pBuf1[IndexRow1 + x + 1].Z + pBuf1[IndexRow1Up + x].Z + pBuf1[IndexRow1Down + x].Z);
        }
    }

    TArray<FColor> Colors;
    Colors.SetNum(NumVertices);
    TArray<FProcMeshTangent> Tangents;
    Tangents.SetNum(NumVertices);

    CalculateNormals();

    DynamicMesh->CreateMeshSection(0,
        Vertices[CurrentVertexIndex],
        Triangles,
        Normals,
        Texcoords,
        Colors,
        Tangents, true);
}

void AWaterMeshActor::Push(float x, float y, float depth, bool absolute)
{
	if (x > MeshComplexity || y > MeshComplexity)
	{
		return;
	}

    TArray<FVector>& pBuf = Vertices[CurrentVertexIndex];

    depth = depth * 0.01 * 100;

#define _PREP(addx, addy) { \
    int PointIndex = (int)(y+addy) * (MeshComplexity+1) + (int)(x+addx); \
    float diffy = y - floor(y + addy); \
    float diffx = x - floor(x + addx); \
    float dist = sqrt(diffy * diffy + diffx* diffx); \
    float power = 1 - dist; \
    if(power < 0) \
        power = 0; \
    if(absolute) \
        pBuf[PointIndex].Z = depth * power; \
    else \
        pBuf[PointIndex].Z += depth * power; \
    }

    _PREP(0, 0);
    _PREP(0, 1);
    _PREP(1, 0);
    _PREP(1, 1);

#undef _PREP
}

void AWaterMeshActor::SetRippleSpeed(float fInput)
{
    ParamC = fInput;
}

void AWaterMeshActor::SetDistance(float fInput)
{
    ParamD = fInput;
}

void AWaterMeshActor::SetViscosity(float fInput)
{
    ParamU = fInput;
}

void AWaterMeshActor::SetFrameTime(float fInput)
{
    ParamT = fInput;
}