// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "WaterMeshActor.generated.h"

class UProceduralMeshComponent;

UCLASS()
class AWaterMeshActor : public AActor
{
	GENERATED_BODY()

    UPROPERTY(VisibleAnywhere, Category = "Water")
    UProceduralMeshComponent* DynamicMesh;
	
public:	
    // 网格复杂度
    //UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Water)
    int32 MeshComplexity;

    // 网格尺寸
    //UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Water)
    int32 PlaneSize;

    // Ripple Speed
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Water)
    float ParamC;

    // Distance
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Water)
    float ParamD;

    // viscosity
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Water)
    float ParamU;

    // time
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Water)
    float ParamT;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Water)
    uint8 bUseFakeNormals;

    UFUNCTION(BlueprintCallable, Category = Water)
    void Push(float x, float y, float depth, bool absolute);

    // 参数范围[0,2]
    UFUNCTION(BlueprintCallable, Category = Water)
        void SetRippleSpeed(float fInput);

    // 参数范围[0.1,5.0]
    UFUNCTION(BlueprintCallable, Category = Water)
        void SetDistance(float fInput);

    // 参数范围[0,1]
    UFUNCTION(BlueprintCallable, Category = Water)
        void SetViscosity(float fInput);

    // 参数范围[0,1]
    UFUNCTION(BlueprintCallable, Category = Water)
        void SetFrameTime(float fInput);

	// Sets default values for this actor's properties
	AWaterMeshActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

    void GenerateMesh();
    void CalculateNormals();
    void UpdateMesh(float DeltaSeconds);

private:
    
    

    

    // 面数总和
    int32 NumFaces;

    // 顶点数
    int32 NumVertices;

    // 保存顶点
    TArray<FVector> Vertices[3];

    // 保存计算出的Normal
    TArray<FVector> Normals;

    // 保存UV
    TArray<FVector2D> Texcoords;

    // 保存三角形
    TArray<int32> Triangles;

    // 当前使用哪一个顶点缓冲区
    int CurrentVertexIndex;
};
