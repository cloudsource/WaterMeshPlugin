// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System;
using System.IO;


namespace UnrealBuildTool.Rules
{
    public class WaterMesh : ModuleRules
    {
        public WaterMesh(TargetInfo Target)
        {
            PublicIncludePaths.AddRange(
                new string[] {
                
                }
                );

            PrivateIncludePaths.AddRange(
                new string[] {
                    "WaterMesh/Private",
					// ... add other private include paths required here ...
				}
                );

            PublicDependencyModuleNames.AddRange(
                new string[] {
                    "Core",
                    "CoreUObject",
                    "Engine",
                    "InputCore",
                    //"HeadMountedDisplay",
                    "RHI",
                    "RenderCore",
                    "SlateCore",
                    "Slate",
                    "ProceduralMeshComponent"
                }
                );

            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
					// ... add private dependencies that you statically link with here ...
				}
                );

            DynamicallyLoadedModuleNames.AddRange(
                new string[]
                {
					// ... add any modules that your module loads dynamically here ...
				}
                );
        }
    }
}